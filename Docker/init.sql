-- Создание таблицы для электронных адресов
CREATE TABLE IF NOT EXISTS emails (
    id SERIAL PRIMARY KEY,
    email VARCHAR(255) NOT NULL
);

-- Создание таблицы для номеров телефонов
CREATE TABLE IF NOT EXISTS phone_numbers (
    id SERIAL PRIMARY KEY,
    phone_number VARCHAR(20) NOT NULL
);

-- Вставка тестовых данных для таблицы emails
INSERT INTO emails (email) VALUES
    ('test1@example.com'),
    ('test2@example.com'),
    ('test3@example.com');

-- Вставка тестовых данных для таблицы phone_numbers
INSERT INTO phone_numbers (phone_number) VALUES
    ('89805626153'),
    ('+79109010105'),
    ('89209567611');
