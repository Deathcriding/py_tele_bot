#!/bin/bash
sleep 20
pg_ctl stop
rm -rf /var/lib/postgresql/data/*
pg_basebackup -R -h 172.20.0.2 -U repl_user -D /var/lib/postgresql/data -P
pg_ctl start
