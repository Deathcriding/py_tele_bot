#!/bin/bash

# Изменение конфигурации PostgreSQL
sed -i "s/^#*\(log_replication_commands *= *\).*/\1on/" /var/lib/postgresql/data/postgresql.conf
sed -i "s/^#*\(archive_mode *= *\).*/\1on/" /var/lib/postgresql/data/postgresql.conf
sed -i "s|^#*\(archive_command *= *\).*|\1'cp %p /oracle/pg_data/archive/%f'|" /var/lib/postgresql/data/postgresql.conf
sed -i "s/^#*\(max_wal_senders *= *\).*/\110/" /var/lib/postgresql/data/postgresql.conf
sed -i "s/^#*\(wal_level *= *\).*/\1replica/" /var/lib/postgresql/data/postgresql.conf
sed -i "s/^#*\(wal_log_hints *= *\).*/\1on/" /var/lib/postgresql/data/postgresql.conf

# Добавление строки в pg_hba.conf для разрешения репликации
echo 'host replication repl_user 172.20.0.2/24 trust' >> /var/lib/postgresql/data/pg_hba.conf
# Создание пользователя для репликации в PostgreSQL
psql -U $PSQL_USER -d $PSQL_DATABASE -c "CREATE USER repl_user WITH REPLICATION LOGIN PASSWORD '$PSQL_PASSWORD';"
pg_ctl restart
