from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import re
import urllib3
import paramiko
import psycopg2
from psycopg2 import Error
import os
from dotenv import load_dotenv

urllib3.disable_warnings()
load_dotenv()


def execute_command(command):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname=os.getenv('HOST'), username=os.getenv('USERNAME1'), password=os.getenv('PASSWORD'),
                   port=os.getenv('PORT'))
    stdin, stdout, stderr = client.exec_command(command)
    data = stdout.read() + stderr.read()
    client.close()
    data = str(data).replace('\\n', '\n').replace('\\t', '\t')[2:-1]
    return data


TOKEN = os.getenv('TOKEN')


def split_message(text, max_length=4096):
    if len(text) <= max_length:
        return [text]  # Если текст короче максимальной длины, возвращаем его как есть

    parts = []
    current_part = ""
    for word in text.split():
        if len(current_part) + len(word) + 1 <= max_length:  # +1 для пробела между словами
            current_part += word + " "
        else:
            parts.append(current_part.strip())  # Добавляем часть в список и сбрасываем текущую часть
            current_part = word + " "

    if current_part:
        parts.append(current_part.strip())  # Добавляем оставшуюся часть

    return parts


# Функция для поиска номеров телефонов в тексте
def find_phone_numbers(text):
    # Регулярное выражение для поиска номеров телефонов
    phone_regex = re.compile(r'(?:(\+7|8)[\s\-]?\(?(\d{3})\)?[\s\-]?(\d{3})[\s\-]?(\d{2})[\s\-]?(\d{2}))')

    # Находим все совпадения в тексте
    phone_numbers = phone_regex.findall(text)

    return phone_numbers


# Функция для поиска email адресов в тексте
def find_emails(text):
    # Регулярное выражение для поиска email адресов
    email_regex = re.compile(r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b')

    # Находим все совпадения в тексте
    emails = email_regex.findall(text)

    return emails


def find_pass(text):
    # Регулярное выражение для поиска паролей
    pass_regex = re.compile(r'^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[!@#$%^&*()]).{8,}$')
    passwords = pass_regex.findall(text)

    return passwords


# Функция для обработки команды /find_phone_number
def find_phone_number(update, context):
    # Запрос текста у пользователя
    update.message.reply_text("Введите текст для поиска номеров телефонов:")

    # Устанавливаем следующее ожидаемое сообщение от пользователя в состояние "WAITING_FOR_PHONE_TEXT"
    context.user_data['state'] = 'WAITING_FOR_PHONE_TEXT'


# Функция для обработки команды /find_email
def find_email(update, context):
    # Запрос текста у пользователя
    update.message.reply_text("Введите текст для поиска email адресов:")

    # Устанавливаем следующее ожидаемое сообщение от пользователя в состояние "WAITING_FOR_EMAIL_TEXT"
    context.user_data['state'] = 'WAITING_FOR_EMAIL_TEXT'


# Функция для обработки команды /verify_password
def verify_password(update, context):
    # Запрос текста у пользователя
    update.message.reply_text("Введите пароль для проверки:")

    # Устанавливаем следующее ожидаемое сообщение от пользователя в состояние "WAITING_FOR_EMAIL_TEXT"
    context.user_data['state'] = 'WAITING_FOR_PASSWORD_TEXT'


# Функция для обработки команды /get_release
def get_release(update, context):
    update.message.reply_text('Информация о релизе\n' + execute_command('uname -r'))


# Функция для обработки команды /get_uname
def get_uname(update, context):
    update.message.reply_text(
        execute_command('Информация об архитектуры процессора, имени хоста системы и версии ядра\n' + 'uname -rmn'))


# Функция для обработки команды /uptime
def uptime(update, context):
    update.message.reply_text('Информация о времени работы\n' + execute_command('uptime'))


# Функция для обработки команды /get_df
def get_df(update, context):
    update.message.reply_text('Состояние файловой системы\n' + execute_command('df -h'))


# Функция для обработки команды /get_df
def get_free(update, context):
    update.message.reply_text('Состояние оперативной памяти\n' + execute_command('free -h'))


# Функция для обработки команды /get_mpstat
def get_mpstat(update, context):
    update.message.reply_text('Производительность системы\n' + execute_command('mpstat'))


# Функция для обработки команды /get_w
def get_w(update, context):
    update.message.reply_text('Информация о работающих пользователях\n' + execute_command('w'))


# Функция для обработки команды /get_auths
def get_auths(update, context):
    update.message.reply_text('Информация о последних входах\n' + execute_command('last -n 10'))


# Функция для обработки команды /get_critical
def get_critical(update, context):
    update.message.reply_text(
        'Информация о последних критических событиях\n' + execute_command('tail -n 5 /var/log/syslog'))


# Функция для обработки команды /get_ps
def get_ps(update, context):
    for message in split_message(execute_command('ps -aux')):
        update.message.reply_text(message)


# Функция для обработки команды /get_ss
def get_ss(update, context):
    for message in split_message(execute_command('ss -tulnp')):
        update.message.reply_text(message)


# Функция для обработки команды /get_services
def get_services(update, context):
    for message in split_message(execute_command('service --status-all')):
        update.message.reply_text(message)


# Функция для обработки команды /get_apt_list
def get_apt_list(update, context):
    if len(update.message.text.split()) > 1 and update.message.text.split()[0] == '/get_apt_list':
        package_name = update.message.text.split()[1]
        if not any(char in package_name for char in [';', '|', '&', '`', '<', '>', '!', '$']):
            result = execute_command('apt list ' + package_name)
            update.message.reply_text(result)
            print('apt list ' + package_name)
        else:
            update.message.reply_text("Недопустимые символы в названии пакета.")
    else:
        result = execute_command('apt list --installed')
        for message in split_message(result):
            update.message.reply_text(message)


# Функция для обработки текстовых сообщений
def handle_text_message(update, context):
    text = update.message.text

    # Проверяем состояния ответа пользователя
    if context.user_data.get('state') == 'WAITING_FOR_PHONE_TEXT':
        # Поиск номеров телефонов в тексте
        phone_numbers = find_phone_numbers(text)
        if phone_numbers:
            numbers = [f'{i + 1}) {"".join(phone_numbers[i])}' for i in range(len(phone_numbers))]
            update.message.reply_text(
                "Найденные номера телефонов:\n" + "\n".join(numbers) + f'\nВсего найдено {len(numbers)} телефонов')
        else:
            update.message.reply_text("Номера телефонов не найдены.")

        # Сбрасываем состояние пользователя
        del context.user_data['state']
    elif context.user_data.get('state') == 'WAITING_FOR_EMAIL_TEXT':
        # Поиск email адресов в тексте
        emails = find_emails(text)
        if emails:
            email = [f'{i + 1}) {"".join(emails[i])}' for i in range(len(emails))]
            update.message.reply_text(
                "Найденные email адреса:\n" + "\n".join(email) + f'\nВсего найдено {len(email)} Email')
        else:
            update.message.reply_text("Email адреса не найдены.")

        # Сбрасываем состояние пользователя
        del context.user_data['state']
    elif context.user_data.get('state') == 'WAITING_FOR_PASSWORD_TEXT':
        # Поиск паролей
        if find_pass(text):
            update.message.reply_text('Пароль норм')
        else:
            update.message.reply_text('Пароль говно')
        del context.user_data['state']
    else:
        update.message.reply_text("Дядя Петя, ты дурак?")


# Функция для запуска бота
def main():
    updater = Updater(TOKEN, use_context=True)

    # Получаем диспетчер для регистрации обработчиков
    dp = updater.dispatcher

    # Регистрируем обработчики команд
    dp.add_handler(CommandHandler("find_phone_number", find_phone_number))
    dp.add_handler(CommandHandler("find_email", find_email))
    dp.add_handler(CommandHandler("verify_password", verify_password))
    dp.add_handler(CommandHandler("get_release", get_release))
    dp.add_handler(CommandHandler("get_uname", get_uname))
    dp.add_handler(CommandHandler("uptime", uptime))
    dp.add_handler(CommandHandler("get_df", get_df))
    dp.add_handler(CommandHandler("get_free", get_free))
    dp.add_handler(CommandHandler("get_mpstat", get_mpstat))
    dp.add_handler(CommandHandler("get_w", get_w))
    dp.add_handler(CommandHandler("get_auths", get_auths))
    dp.add_handler(CommandHandler("get_critical", get_critical))
    dp.add_handler(CommandHandler("get_ps", get_ps))
    dp.add_handler(CommandHandler("get_ss", get_ss))
    dp.add_handler(CommandHandler("get_services", get_services))
    dp.add_handler(CommandHandler("get_apt_list", get_apt_list))
    # Регистрируем обработчик текстовых сообщений
    dp.add_handler(MessageHandler(Filters.text & ~Filters.command, handle_text_message))

    # Запускаем бота
    updater.start_polling()
    updater.idle()


# Запускаем бота
if __name__ == '__main__':
    main()
